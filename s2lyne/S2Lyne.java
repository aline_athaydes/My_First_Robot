package s2lyne;
import robocode.*;
import java.awt.Color;

/**
 * S2Lyne - a robot by (Aline Athaydes)
 */
public class S2Lyne extends AdvancedRobot
{
	/**
	 * run: S2Lyne's default behavior
	 */
	boolean movingForward;
	
	
	public void run() {
		
		setBodyColor(new Color(0x45, 0x8B, 0x74));
		setGunColor(new Color(0x45, 0x8B, 0x74));
		setRadarColor(new Color(0, 100, 100));
		setBulletColor(new Color(255, 255, 100));
		setScanColor(new Color(0xCA, 0xFF, 0x70));

		while (true) {
				setAhead(40000);
				movingForward = true;
				setTurnRight(90);
				waitFor(new TurnCompleteCondition(this));
				setTurnLeft(180);
				waitFor(new TurnCompleteCondition(this));
				setTurnRight(180);
				waitFor(new TurnCompleteCondition(this));
		}				
		
	}
	public void onScannedRobot(ScannedRobotEvent e) {
		fire(1);
	}
	public void onHitByBullet(HitByBulletEvent e) {
		back(10);
		fire(1);
	}
	
	public void onHitRobot(HitRobotEvent e) {
		if (e.isMyFault()) {
			reverseDirection();
		}
	}

	public void onHitWall(HitWallEvent e) {
		reverseDirection();
	}	
	
	public void reverseDirection() {
		if (movingForward) {
			setBack(2000);
			movingForward = false;
		} else {
			setAhead(2000);
			movingForward = true;
		}
	}
}
